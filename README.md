# ssin
Short scale in numbers.

# Install

```console
$ npm install ssin
```

# Usage

```js
const ssin = require('ssin')

ssin(123)
// => [ '123', '' ]

ssin(1234567)
// => [ '1', 'million' ]

ssin(1234567, 1)
// => [ '1.2', 'million' ]

ssin(1234567, 2)
// => [ '1.23', 'million' ]

ssin(123456789012345678901234567890, 3)
// => [ '123.456', 'octillion' ]
```
